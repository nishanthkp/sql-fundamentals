# Exercise-6 Solutions

### EXERCISE: ORDER BY SINGLE COLUMNS

Get people, sort by name.
```sql
SELECT name
FROM people ORDER BY name;
```

Get people, in order of when they were born.
```sql
SELECT birthdate, name
FROM people
ORDER BY birthdate;
```

Get films released in 2000 or 2015, in the order they were released.
```sql
SELECT title, release_year
FROM films
WHERE release_year = '2000' or release_year = '2015'
ORDER BY release_year;
```

Get all films except those released in 2015, order them so we can see results.
```sql
SELECT *
FROM films
WHERE release_year <> 2015
ORDER BY release_year;
```



Get the score and film id for every film, from highest to lowest.
```sql
SELECT imdb_score, film_id
FROM reviews
___ ___ imdb_score ___;
```
```sql
SELECT imdb_score, film_id
FROM reviews
ORDER BY imdb_score DESC;
```

Get the titles of films in reverse order.
```sql
SELECT *
FROM films
ORDER BY title DESC;
```

### EXERCISE: ORDER BY MULTIPLE COLUMNS


Get people, in order of when they were born, and alphabetical order.
```sql
SELECT birthdate, name
FROM people
ORDER BY birthdate, name;
```

Get films from in 2000 or 2015, sorted in the order they were released, and how long they were.
```sql
SELECT release_year, duration, title
FROM films
WHERE release_year = '2000' or release_year = '2015'
ORDER BY release_year, duration;
```

Get films between 2000 and 2015, sorted by certification and the year they were released.
```sql
SELECT certification, release_year, title
FROM films
WHERE release_year = '2000' or release_year = '2015'
ORDER BY certification, release_year;
```

Get people whose names start with A, B or C, (redundantly) ordered.
```sql
SELECT name, birthdate
FROM people
WHERE name LIKE 'A%' OR name LIKE 'B%' OR name LIKE 'C%'
ORDER BY birthdate;
```
