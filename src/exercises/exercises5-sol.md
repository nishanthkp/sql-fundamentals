# LIKE Solutions

Get people whose names begin with 'B'.
```sql
SELECT name
FROM people
WHERE name LIKE 'B%';
```

Get people whose names begin with 'Br'.
```sql
SELECT name
FROM people
WHERE name LIKE 'Br%';
```

Get people whose names have 'r' as the second letter.
```sql
SELECT name
FROM people
WHERE name LIKE '_r%';
```

Get people whose names don't start with A.
```sql
SELECT name
FROM people
WHERE name NOT LIKE 'A%';
```
