# Exercise 2 Solutions


Get the title of every film.
```sql
SELECT title
FROM films;
```

Get all details for every film.
```sql
SELECT *
FROM films;
```

Get the names of everyone involved in working on the films.
```sql
SELECT name
FROM people;
```

Get the title and release year of every film.
```sql
SELECT title, release_year
FROM films;
```

Get the title, release year and country for every film.
```sql
SELECT title, release_year, country
FROM films;
```

Get every person's name and their date of birth where possible.
```sql
SELECT name, birthdate
FROM people;
```

Get every person name and their date of death where possible.
```sql
SELECT name, deathdate
FROM people;
```

Get everyone's name, date of birth, and date of death (where possible).
```sql
SELECT name, birthdate, deathdate
FROM people;
```

Get all the different countries.

```sql
SELECT DISTINCT country
FROM films;
```

Get all the different film languages.
```sql
SELECT DISTINCT language
FROM films;
```

Get the different types of film roles.
```sql
SELECT DISTINCT role
FROM roles;
```

Get all the different certification categories.
```sql
SELECT DISTINCT certification
FROM films;
```

Get all the different IMDB scores - has any scored over 9.5?
```sql
SELECT DISTINCT imdb_score
FROM reviews;
```
