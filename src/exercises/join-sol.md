# Join Solutions

**Work on Postgres Database**

### Inner Join

Join Two Tables

```sql
SELECT *
FROM salespeople
INNER JOIN dealerships
ON salespeople.dealership_id = dealerships.dealership_id 
ORDER BY 1;
```

Extend the Above Query to filter dealerships state who belong to 'CA'

```sql
SELECT *
FROM salespeople
INNER JOIN dealerships
ON salespeople.dealership_id = dealerships.dealership_id
WHERE dealerships.state = 'CA'
ORDER BY 1
```

Use salespeople with s and dealerships with d, Aliases and Write the Above Query

```sql
SELECT s.*
FROM salespeople s
INNER JOIN dealerships d
ON d.dealership_id = s.dealership_id
WHERE d.state = 'CA'
ORDER BY 1;
```

Alternative Method to Create Alias using AS Keyword

```sql
SELECT s.*
FROM salespeople AS s
INNER JOIN dealerships AS d
ON d.dealership_id = s.dealership_id
WHERE d.state = 'CA'
ORDER BY 1;
```

### Outer Join

```sql
SELECT *
FROM customers c
LEFT OUTER JOIN emails e ON e.customer_id=c.customer_id
ORDER BY c.customer_id
LIMIT 50;
```

When you look at the output of the query, you should see that entries from the customer table are present. 

However, for some of the rows, such as for customer row 27 which can be seen in output,

the columns belonging to the emails table are completely full of nulls. 

This arrangement explains how the outer join is different from the inner join. 

If the inner join was used, the customer_id column would not be blank. 

This query, however, is still useful because we can now use it to find people who have never received an email.



Because those customers who were never sent an email have a null customer_id column in the emails table, 

we can find all of these customers by checking the customer_id column in the emails table as follows:

```sql
SELECT *
FROM customers c
LEFT OUTER JOIN emails e ON c.customer_id = e.customer_id 
WHERE e.customer_id IS NULL
ORDER BY c.customer_id
LIMIT 50;
```
A right outer join is very similar to a left join, except the table on the "right" (the second listed table) will now have every row show up, and the "left" table will have NULLs if the join condition is not met. 

To illustrate, let's "flip" the last query by right-joining the emails table to the customers table with the following query:

```sql
SELECT *
FROM emails e
RIGHT OUTER JOIN customers c ON e.customer_id=c.customer_id
ORDER BY c.customer_id
LIMIT 50;
```


Notice that this output is similar to what was produced in output except that the data from the emails table is now on the left-hand side, and the data from the customers table is on the right-hand side. 

Once again, customer_id 27 has NULL for the email. This shows the symmetry between a right join and a left join.

Finally, there is the full outer join. 

The full outer join will return all rows from the left and right tables, regardless of whether the join predicate is matched. For rows where the join predicate is met, the two rows are combined in a group. 

For rows where they are not met, the row has NULL filled in. 

The full outer join is invoked by using the FULL OUTER JOIN clause, followed by a join predicate. 

Here is the syntax of this join:

```sql
SELECT * 
FROM email e
FULL OUTER JOIN customers c
ON e.customer_id=c.customer_id;
```

