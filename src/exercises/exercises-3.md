# COUNT

1. Count the number of rows in the people table.
1. Count the number of birthdate entries in the people table.
1. Count the number of unique birthdate entries in the people table.
1. Count the number of unique languages.
1. Count the number of unique countries.
1. Count the total number of people.
1. Count the number of people who have died.

