# Exercises

### Exercise - 1

* Complete Section 3 - SELECT Query
* Complete till Section 4.8 - WHERE Clause 

### Exercise - 2

1. Get title of Every film
2. Get all details for Every Film
3. Get the Names of Everyone Invovled in working on all films
4. Get the title and release year for Every Film.
5. Get the title, release year and country for every film.
6. Get every person's name and their date of birth where possible.
7. Get all the different countries uniqely
8. Get all the different film languages.