# Exercise 3 Solutions

Count the number of rows in the people table.
```sql
SELECT COUNT(*)
FROM people;
```

Count the number of birthdate entries in the people table.
```sql
SELECT COUNT(birthdate)
FROM people;
```

Count the number of unique birthdate entries in the people table.
```sql
SELECT COUNT(DISTINCT birthdate)
FROM people;
```

Count the number of unique languages.
```sql
SELECT COUNT(DISTINCT language)
FROM films;
```

Count the number of unique countries.
```sql
SELECT COUNT(DISTINCT country)
FROM films;
```

Count the total number of people.
```sql
SELECT COUNT(*)
FROM people;
```

Count the number of people who have died.
```sql
SELECT COUNT(deathdate)
FROM people;
```

Count the number of years the dataset covers.
```sql
SELECT COUNT(DISTINCT release_year)
FROM films
```
