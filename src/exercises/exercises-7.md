# Group By


### EXERCISE: GROUP BY SINGLE COLUMN
1. Get count of films made in each year.


1. Get count of films, group by release year then order by release year.


1. Get count of films released in each year, ordered by count, lowest to highest.


1. Get count of films released in each year, ordered by count highest to lowest.


1. Get lowest box office earnings per year.


1. Get the total amount made in each language.


1. Get the total amount spent by each country.


### EXERCISE: GROUP BY MULTIPLE COLUMNS
1. Get the most spent making a film for each year, for each country.


1. Get the lowest box office made by each country in each year.

