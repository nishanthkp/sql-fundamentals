# HAVING

use **films-db-netflix.db database**

1. Get the rounded average budget and average box office earnings for movies since 1990,but only if the average budget was greater than $20m in that year


1. Get the name, average budget, average box office take of countries who have made more than 10 films. Order by name, and get the top five.
