# HAVING Solutions

Get the rounded average budget and average box office earnings for movies since 1990, 

but only if the average budget was greater than $20m in that year

```sql
select release_year, round(avg(budget)) as avg_budget, round(avg(gross)) as avg_gross_box_office
from films
where release_year >= 1990
group by release_year
having avg(budget) > 20000000
```

Get the name, average budget, average box office take of countries who have made more than 10 films. 

Order by name, and get the top five.


```sql
SELECT country, ROUND(AVG(budget)) AS avg_budget, ROUND(AVG(gross)) AS avg_box_office
FROM films
GROUP BY country
HAVING COUNT(title) > 10
ORDER BY country
LIMIT 5;
```
