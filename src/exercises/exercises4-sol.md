# Exercise 4 Solutions


Get the average duration of all films, rounded to the nearest minute.
```sql
SELECT ROUND(AVG(duration))
AS rounded_avg_run_time
FROM films;
```

Get the average duration of all films, rounded down to nearest minute.
```sql
SELECT FLOOR(AVG(duration))
AS floored_avg_run_time
FROM films;
```

Get the average duration of all films, rounded up to the nearest minute.
```sql
SELECT CEIL(AVG(duration))
FROM films;;
```
