# GROUP BY

### EXERCISE: GROUP BY SINGLE COLUMN
Get count of films made in each year.
```sql
SELECT release_year, COUNT(release_year)
FROM films
GROUP BY release_year;
```

Get count of films, group by release year then order by release year.
```sql
SELECT release_year, COUNT(title) as films_released
FROM films
GROUP BY release_year
ORDER BY release_year;
```

Get count of films released in each year, ordered by count, lowest to highest.
```sql
SELECT release_year, COUNT(title) AS films_released
FROM films
GROUP BY release_year
ORDER BY films_released;
```

Get count of films released in each year, ordered by count highest to lowest.
```sql
SELECT release_year, COUNT(title) AS films_released
FROM films
GROUP BY release_year
ORDER BY films_released DESC;
```

Get lowest box office earnings per year.
```sql
SELECT release_year, MIN(gross)
FROM films
GROUP BY release_year
ORDER BY release_year;
```

Get the total amount made in each language.
```sql
SELECT language, SUM(gross)
FROM films
GROUP BY language;
```

Get the total amount spent by each country.
```sql
SELECT country, SUM(gross)
FROM films
GROUP BY country;
```

### EXERCISE: GROUP BY MULTIPLE COLUMNS
Get the most spent making a film for each year, for each country.
```sql
SELECT release_year, country, MAX(budget)
FROM films
GROUP BY release_year, country
ORDER BY release_year, country;
```

Get the lowest box office made by each country in each year.
```sql
SELECT release_year, country, MIN(gross)
FROM films
GROUP BY release_year, country
ORDER BY release_year, country;
```
