# Order By

### EXERCISE: ORDER BY SINGLE COLUMNS

1. Get people, sort by name.
1. Get people, in order of when they were born.
1. Get films released in 2000 or 2015, in the order they were released.
1. Get all films except those released in 2015, order them so we can see results.

### EXERCISE: ORDER BY MULTIPLE COLUMNS


1. Get people, in order of when they were born, and alphabetical order.
1. Get films from in 2000 or 2015, sorted in the order they were released, and how long they were.
1. Get films between 2000 and 2015, sorted by certification and the year they were released.
1. Get people whose names start with A, B or C, (redundantly) ordered.

