# Self Assessment Challenges and Solutions

The CEO, COO, and CFO of AlRajhi Bank would like to gain some insights on what might be driving sales.

Now that the company feels they have a strong enough analytics team with your arrival. 

The task has been given to you, and your boss has informed that this project is the most important project the Data team has worked on:


1. Fetch all customers that have a phone number ordered by the date the customer was added to the database

```sql
SELECT *
FROM customers
WHERE phone IS NOT NULL
ORDER BY date_added;
```

1. Checking whether every row has a unique customer ID

```sql
SELECT COUNT (DISTINCT customer_id)=COUNT(*) AS equal_ids
FROM customers;
```

1. Calculate the total number of unit sales the company has done.

```sql
SELECT COUNT(*)
FROM sales;
```

1. Calculate the total sales amount in dollars for each state.

```sql
SELECT c.state, SUM(sales_amount) as total_sales_amount
FROM sales s
INNER JOIN customers c ON c.customer_id=s.customer_id
GROUP BY 1
ORDER BY 1;
```

1. Calculate the total sales amount for all individual months in 2018

```sql
SELECT sales_transaction_date::DATE,
SUM(sales_amount) as total_sales_amount
FROM sales
WHERE sales_transaction_date>='2018-01-01'
AND sales_transaction_date<'2019-01-01'
GROUP BY 1
ORDER BY 1;
```

```sql
select cast(extract(month from sales_transaction_date) as integer) as month_in_2018 ,sum(sales_amount) as total_sales_amount
from sales
where sales_transaction_date >= '2018-01-01 00:00:00' and sales_transaction_date < '2019-01-01 00:00:00'
group by month_in_2018
order by month_in_2018
```


1. Identify the top five best dealerships in terms of the most units sold (ignore internet sales).

```sql
SELECT s.dealership_id, COUNT(*)
FROM sales s
WHERE channel='dealership'
GROUP BY 1
ORDER BY 2 DESC
LIMIT 5
```


1. Determine a list of customers who had been sent an email, including information for the subject of the email and whether they opened and clicked on the email. The resulting table should include the customer_id, first_name, last_name, email_subject, opened, and clicked columns

```sql
SELECT customers.customer_id, customers.first_name, customers.last_name, emails.opened, emails.clicked 
FROM customers INNER JOIN emails ON customers.customer_id=emails.customer_id;
```

1. Find the customers who have a dealership in their city. Customers who do not have a dealership in their city should have a blank value for the city column

```sql
SELECT customers.customer_id, customers.first_name, customers.last_name, customers.city 
FROM customers 
LEFT JOIN dealerships on customers.city=dealerships.city;
```

1. List those customers who do not have dealers in their city

```sql
SELECT customers.customer_id, customers.first_name, customers.last_name, customers.city 
FROM customers 
LEFT JOIN dealerships on customers.city=dealerships.city
where customers.city is NULL;
```

1. The head of sales at your company would like a list of all customers who bought a car. We need to create a query that will return all customer IDs, first names, last names, and valid phone numbers of customers who purchased a car.

```sql
SELECT 
c.customer_id, c.first_name,
c.last_name, c.phone
FROM 
sales s
INNER JOIN 
customers c ON c.customer_id=s.customer_id
INNER JOIN 
products p ON p.product_id=s.product_id
where p.product_type='automobile' AND c.phone IS NOT NULL;
```

1. We want to visualize the addresses of dealerships and customers using Google Maps. To do this, you would need both the addresses of customers and dealerships.

**Step 1**

```sql
SELECT 
street_address, city, state, postal_code
FROM 
customers
WHERE 
street_address IS NOT NULL;
```

**Step 2**

```sql
select
street_address, city, state, postal_code
FROM 
dealerships
WHERE 
street_address IS NOT NULL;
```

**Combine Both the Queries**

```sql
(
SELECT 
street_address, city, state, postal_code
FROM 
customers
WHERE 
street_address IS NOT NULL
)
UNION
(
SELECT 
street_address, city, state, postal_code
FROM 
dealerships
WHERE 
street_address IS NOT NULL
)
ORDER by 1;
```

1. In order to help build up marketing awareness for the new Model Chi, the marketing team would like to throw a party for some of AlRajhi wealthiest customers in Los Angeles, CA. To help facilitate the party, they would like you to make a guest list with AlRajhi customers who live in Los Angeles, CA, as well as salespeople who work at the AlRajhi dealership in Los Angeles, CA. The guest list should include first and last names and whether the guest is a customer or an employee.


```sql
(
SELECT 
first_name, last_name, 'Customer' as guest_type
FROM 
customers
where city='Los Angeles' AND state='CA'
)
UNION
(
select first_name, last_name, 'Employee' as guest_type
from salespeople s
INNER JOIN 
dealerships d ON d.dealership_id=s.dealership_id
where d.city='Los Angeles' AND d.state='CA'
)
```
1. We want to return all rows for customers from the customers table. Additionally, you would like to add a column that labels a user as being an Elite Customer type if they live in postal code 33111, or as a Premium Customer type if they live in postal code 33124. Otherwise, it will mark the customer as a Standard Customer type.

```sql
select *,
CASE WHEN postal_code='33111' THEN 'Elite Customer'
WHEN postal_code='33124' THEN 'Premium Customer'
ELSE 'Standard Customer'
END
AS customer_type
FROM customers;
```

1. The head of sales has an idea to try and create specialized regional sales teams that will be able to sell scooters to customers in specific regions, as opposed to generic sales teams.
To make his idea a reality, he would like a list of all customers mapped to regions.

For customers from the states of MA, NH, VT, ME, CT, or RI, he would like them labeled as 'New England'

For customers from the states of GA, FL, MS, AL, LA, KY, VA, NC, SC, TN, VI, WV, or AR, he would like the customers labeled as 'Southeast'

Customers from any other state should be labeled as Other


```sql
SELECT 
c.customer_id,
case
WHEN c.state in ( 'MA', 'NH', 'VT', 'ME', 'CT', 'RI') THEN 'New England'
WHEN c.state in ( 'GA', 'FL', 'MS', 'AL', 'LA', 'KY', 'VA', 'NC', 'SC', 'TN', 'VI', 'WV', 'AR') THEN 'Southeast'
ELSE 'Other'
END as region
from customers c
ORDER by 1;
```

### Database Design Challenges

Create the table with New York City customers

```sql
CREATE TABLE customers_nyc AS (
SELECT * FROM 
customers
where city='New York City'
and state='NY');
```




