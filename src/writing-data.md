
# Writing Data

In this section, we will learn how to write, modify, and delete data in a database.


## Using `INSERT`

To create a new record in a table, use the `INSERT` command and supply the values for the needed columns.

Put yourself into the `ATTENDEE` table.

```sql
INSERT INTO ATTENDEE (FIRST_NAME, LAST_NAME)
VALUES ('Thomas','Nield')
```

Notice above that we declare the table we are writing to, which is `ATTENDEE`. 

Then we declare the columns we are supplying values for `(FIRST_NAME, LAST_NAME)`, followed by the values for this new record `('Thomas','Nield')`.

Notice we did not have to supply a value for `ATTENDEE_ID` as we have set it in the previous section to generate its own value. 

`PHONE`, `EMAIL`, and `VIP` fields have default values or are nullable, and therefore optional.


## Multiple `INSERT` records

You can insert multiple rows in an `INSERT`. This will add three people to the `ATTENDEE` table.

```sql
INSERT INTO ATTENDEE (FIRST_NAME, LAST_NAME, PHONE, EMAIL, VIP)
VALUES ('Jon', 'Skeeter', 4802185842,'john.skeeter@rex.net', 1),
  ('Sam','Scala', 2156783401,'sam.scala@gmail.com', 0),
  ('Brittany','Fisher', 5932857296,'brittany.fisher@outlook.com', 0)
```

## Testing the foreign keys

Let's test our design and make sure our primary/foreign keys are working.

Try to `INSERT` a `COMPANY` with a `PRIMARY_CONTACT_ATTENDEE_ID` that does not exist in the `ATTENDEE` table.

```sql
INSERT INTO COMPANY (NAME, DESCRIPTION, PRIMARY_CONTACT_ATTENDEE_ID)
VALUES ('RexApp Solutions','A mobile app delivery service', 5)
```

Currently, there is no `ATTENDEE` with an `ATTENDEE_ID` of 5, this should error out which is good. It means we kept bad data out.

If you use an `ATTENDEE_ID` value that does exist and supply it as a `PRIMARY_CONTACT_ATTENDEE_ID`, we should be good to go.

```sql
INSERT INTO COMPANY (NAME, DESCRIPTION, PRIMARY_CONTACT_ATTENDEE_ID)
VALUES ('RexApp Solutions', 'A mobile app delivery service', 3)
```

### `DELETE` records

The `DELETE` command is dangerously simple. To delete records from both the `COMPANY` and `ATTENDEE` tables, execute the following SQL commands.

```sql
DELETE FROM COMPANY;
DELETE FROM ATTENDEE;
```

Note that the `COMPANY` table has a foreign key relationship with the `ATTENDEE` table. 

Therefore we will have to delete records from `COMPANY` first before it allows us to delete data from `ATTENDEE`. 

Otherwise we will get a "FOREIGN KEY constraint failed effort" due to the `COMPANY` record we just added which is tied to the `ATTENDEE` with the `ATTENDEE_ID` of 3.

You can also use a `WHERE` to only delete records that meet a conditional. To delete all `ATTENDEE` records with no `PHONE` or `EMAIL`, you can run this command.

```sql
DELETE FROM ATTENDEE
WHERE PHONE IS NULL AND EMAIL IS NULL
```

A good practice is to use a `SELECT *` in place of the `DELETE` first. That way you can get a preview of what records will be deleted with that `WHERE` condition.


```sql
SELECT * FROM ATTENDEE
WHERE PHONE IS NULL AND EMAIL IS NULL
```

### `UPDATE` records

Say we wanted to change the phone number for the `ATTENDEE` with the `ATTENDEE_ID` value of 3, which is Sam Scala. We can do this with an `UPDATE` statement.

```sql
UPDATE ATTENDEE SET PHONE = 4802735872
WHERE ATTENDEE_ID = 3
```

Using a `WHERE` is important, otherwise it will update all records with the specified `SET` assignment. This can be handy if you wanted to say, make all `EMAIL` values uppercase.

```sql
UPDATE ATTENDEE SET EMAIL = UPPER(EMAIL)
```

### Dropping Tables

If you want to delete a table, it also is dangerously simple. Be very careful and sure before you delete any table, because it will remove it permanently.

```sql
DROP TABLE MY_UNWANTED_TABLE
```

### Transactions

Transactions are helpful when you want a series of writes to succeed.


Below, we execute two successful write operations within a transaction.

```sql
BEGIN TRANSACTION;

INSERT INTO ROOM (FLOOR_NUMBER, SEAT_CAPACITY) VALUES (9, 80);
INSERT INTO ROOM (FLOOR_NUMBER, SEAT_CAPACITY) VALUES (10, 110);

END TRANSACTION;
```

But if we ever encountered a failure with our write operations, we can call `ROLLBACK` instead of `END TRANSACTION` to go back to the database state when `BEGIN TRANSACTION` was called.

Below, we have a failed operation due to a broken `INSERT`.

```sql
BEGIN TRANSACTION;

INSERT INTO ROOM (FLOOR_NUMBER, SEAT_CAPACITY) VALUES (12, 210);
INSERT INTO ROOM (FLOOR_NUMBER, SEAT_CAPACITY) VALUES (13); --failure
```

So we can call `ROLLBACK` to "rewind" to the database state when `BEGIN TRANSACTION` was called.

```sql
ROLLBACK;
```

### Creating Indexes

You can create an index on a certain column to speed up SELECT performance, such as the `price` column on the `PRODUCT` table.

```sql
CREATE INDEX price_index ON PRODUCT(price);
```

You can also create an index for a column that has unique values, and it will make a special optimization for that case.

```sql
CREATE UNIQUE INDEX name_index ON CUSTOMER(name);
```

To remove an index, use the `DROP` command.

```sql
DROP INDEX price_index;
```


### Working with Dates and Times

Use the ISO 'yyyy-mm-dd' syntax with strings to treat them as dates easily.

Keep in mind much of this functionality is proprietary to SQLite. Make sure you learn the date and time functionality for your specific database platform.

```sql
SELECT * FROM CUSTOMER_ORDER
WHERE SHIP_DATE < '2015-05-21'
```

To get today's date:

```sql
SELECT DATE('now')
```

To shift a date:

```sql
SELECT DATE('now','-1 day')
SELECT DATE('2015-12-07','+3 month','-1 day')
```

To work with times, use `hh:mm:ss` format.

```sql
SELECT '16:31' < '08:31'
```

To get today's GMT time:

```sql
SELECT TIME('now')
```

To shift a time:

```sql
SELECT TIME('16:31','+1 minute')
```

To merge a date and time, use a DateTime type.

```sql
SELECT '2015-12-13 16:04:11'
SELECT DATETIME('2015-12-13 16:04:11','-1 day','+3 hour')
```

To format dates and times a certain way:  

``sql
SELECT strftime('%d-%m-%Y', 'now')
```