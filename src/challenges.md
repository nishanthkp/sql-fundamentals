# Self Assessment Challenges

The CEO, COO, and CFO of AlRajhi Bank would like to gain some insights on what might be driving sales.

Now that the company feels they have a strong enough analytics team with your arrival. 

The task has been given to you, and your boss has informed that this project is the most important project the Data team has worked on:


1. Fetch all customers that have a phone number ordered by the date the customer was added to the database
1. Checking whether every row has a unique customer ID
1. Calculate the total number of unit sales the company has done.
1. Calculate the total sales amount in dollars for each state.
1. Calculate the total sales amount for all individual months in 2018
1. Identify the top five best dealerships in terms of the most units sold (ignore internet sales).

1. Determine a list of customers who had been sent an email, including information for the subject of the email and whether they opened and clicked on the email. The resulting table should include the customer_id, first_name, last_name, email_subject, opened, and clicked columns

1. Find the customers who have a dealership in their city. Customers who do not have a dealership in their city should have a blank value for the city column

1. List those customers who do not have dealers in their city

1. The head of sales at your company would like a list of all customers who bought a car. We need to create a query that will return all customer IDs, first names, last names, and valid phone numbers of customers who purchased a car.

1. Find all the salespeople working in California

1. We want to visualize the addresses of dealerships and customers using Google Maps. To do this, you would need both the addresses of customers and dealerships.


1. In order to help build up marketing awareness for the new Model Chi, the marketing team would like to throw a party for some of AlRajhi wealthiest customers in Los Angeles, CA. To help facilitate the party, they would like you to make a guest list with AlRajhi customers who live in Los Angeles, CA, as well as salespeople who work at the AlRajhi dealership in Los Angeles, CA. The guest list should include first and last names and whether the guest is a customer or an employee.

1. We want to return all rows for customers from the customers table. Additionally, you would like to add a column that labels a user as being an Elite Customer type if they live in postal code 33111, or as a Premium Customer type if they live in postal code 33124. Otherwise, it will mark the customer as a Standard Customer type.

1. The head of sales has an idea to try and create specialized regional sales teams that will be able to sell scooters to customers in specific regions, as opposed to generic sales teams.
To make his idea a reality, he would like a list of all customers mapped to regions.

    For customers from the states of MA, NH, VT, ME, CT, or RI, he would like them labeled as 'New England'

    For customers from the states of GA, FL, MS, AL, LA, KY, VA, NC, SC, TN, VI, WV, or AR, he would like the customers labeled as 'Southeast'

    Customers from any other state should be labeled as Other


### Database Design Challenges
1. Create the table with New York City customers

