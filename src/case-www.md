# Case Statements

### Case Example

Print the Output of the OrderDetails by measuring the Quantity. 

Write a Text Based Column

Quantity > 30 -> 'The quantity is greater than 30'


Quantity = 30 -> 'The quantity is 30'


Any other Value of Quantity -> 'The quantity is under 30'



```sql
SELECT OrderID, Quantity,
CASE
    WHEN Quantity > 30 THEN 'The quantity is greater than 30'
    WHEN Quantity = 30 THEN 'The quantity is 30'
    ELSE 'The quantity is under 30'
END AS QuantityText
FROM Order_Details;
```

Order the customers by City. However, if City is NULL, then order by Country:

```sql
SELECT CustomerName, City, Country
FROM Customers
ORDER BY
(CASE
    WHEN City IS NULL THEN Country
    ELSE City
END);
```
