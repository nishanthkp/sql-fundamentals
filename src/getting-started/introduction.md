# Introduction

Welcome to SQL Fundamentals Course.

In this Course you will gain the fundamental skills you need to interact with and query your data in SQL—a powerful language used by data-driven businesses large and small to explore and manipulate their data to extract meaningful insights. 

In this track, you'll also learn the skills you need to level up your data skills and leave Excel behind you. 

Through hands-on exercises, you’ll discover how to quickly summarize the data and get meaningful insights in to data sets.

What You will learn

- Connecting to a database
- Loading a sample database
- Creating database and table
- Using constraints to ensure data intergrity
- Using SQL SELECT Statement to retrieve data
- Using SQL CREATE Statement to create a table
- Using SQL UPDATE Statement to modify data
- Using SQL DELETE to remove data
- Using SQL DISTINCT to filter duplicate data
- Filter data using SQL WHERE Clause
- Sort data using SQL ORDER BY
- Manipulating data with various operators
- Retrieve and combine data from multiple tables